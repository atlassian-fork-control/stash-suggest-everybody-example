# Stash Suggest Everybody example plugin

This is an example extension of the [Stash Suggest Reviewers plugin](https://bitbucket.org/atlassian/stash-suggest-reviewers).
It indiscriminately suggests all users in Stash as suggested pull request reviewers.

This plugin requires the [Stash Suggest Reviewers plugin](https://bitbucket.org/atlassian/stash-suggest-reviewers) to be
installed in the same Stash server to run.

# How to run

- Install the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Getting+Started).
- Run ``atlas-debug`` from the directory that contains this readme file. This will build the plugin from source and launch
Stash with the plugin deployed.

